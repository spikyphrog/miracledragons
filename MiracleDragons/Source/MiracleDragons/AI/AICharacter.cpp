// Hristo Stoyanov 2021


#include "AICharacter.h"

#include "PatrolAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "MiracleDragons/Character/PlayerCharacter.h"
#include "MiracleDragons/GameModes/MainGameMode.h"
#include "Perception/PawnSensingComponent.h"

// Sets default values
AAICharacter::AAICharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn sensing")); // Creating the pawn sensing component
	PawnSensingComponent->SetPeripheralVisionAngle(75.f); // Setting the peripheral vision angle 
	PawnSensingComponent->OnSeePawn.AddUniqueDynamic(this, &AAICharacter::OnSeePlayer); // Binding the OnSeePawn event to the OnSeePlayer function
}

void AAICharacter::BeginPlay()
{
	Super::BeginPlay();
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
}


void AAICharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GameMode->bHasCompletedAle)
	{
		PawnSensingComponent->SetPeripheralVisionAngle(25.f); // Setting the peripheral vision angle 
	}
}

/**
 * @brief Function that handles what happens if the AI sees the player
 * @param Player 
 */
 void AAICharacter::OnSeePlayer(APawn* Player)
{
	// Creating a reference to the AI controller
	APatrolAIController* AIController = Cast<APatrolAIController>(GetController()); 

	// Check if the AI Controller is valid
	if (AIController)
	{
		// Check the distance between the player and the AI's sight radius
		if (GetDistanceTo(Player) <= PawnSensingComponent->SightRadius)
		{
			// Set the AI's target
			AIController->SetSeenTarget(Player);
		}
	}
}



