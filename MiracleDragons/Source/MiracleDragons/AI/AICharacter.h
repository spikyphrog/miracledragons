// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "AICharacter.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AAICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere)
	class UPawnSensingComponent* PawnSensingComponent; // Declaring the pawn sensing component

	UPROPERTY(EditAnywhere)
	class UBehaviorTree* BehaviorTree; // Declaring the behaviour tree

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class AMainGameMode* GameMode;
	
public:
	// Sets default values for this character's properties
	AAICharacter(); // Constructor
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

protected:
	virtual void BeginPlay() override;
private:
	// Function that is bound to the OnSeePawn event and handles what happens when the AI sees a player
	UFUNCTION()
	void OnSeePlayer(APawn* Player);
};
