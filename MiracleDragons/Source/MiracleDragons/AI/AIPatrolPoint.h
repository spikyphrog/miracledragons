// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "AIPatrolPoint.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AAIPatrolPoint : public ATargetPoint
{
	GENERATED_BODY()
	
};
