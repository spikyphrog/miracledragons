// Hristo Stoyanov 2021


#include "PatrolAIController.h"

#include "AICharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"


APatrolAIController::APatrolAIController()
{
	BehaviourTree = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behaviour Tree")); // Creating the Behaviour tree

	Blackboard = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard")); // Creating the blackboard
}

void APatrolAIController::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(),PatrolPointClass, PatrolPoints); // Filling the patrol points array up
}

/**
 * @brief Function that handles what happen when it possesses an AI character
 * @param InPawn 
 */
 void APatrolAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	// Creating a reference to the AI Character
	AAICharacter* AICharacter = Cast<AAICharacter>(InPawn);

	// Check if the character is valid
	if (AICharacter)
	{
		// Check if it has blackboard asset
		if (AICharacter->BehaviorTree->BlackboardAsset)
		{
			Blackboard->InitializeBlackboard(*AICharacter->BehaviorTree->BlackboardAsset); // Initialise the blackboard
			
			BehaviourTree->StartTree(*AICharacter->BehaviorTree); // Start executing the behaviour tree
		}		
	}
}

/**
 * @brief Function that assigns the BK_TargetToFollow in the blackboard
 * @param Target 
 */
 void APatrolAIController::SetSeenTarget(APawn* Target)
{
	if (Blackboard)
	{
		Blackboard->SetValueAsObject(BK_TargetToFollow, Target);
	}
}

/**
 * @brief Function that returns the array of patrol points
 * @return 
 */
 TArray<AActor*> APatrolAIController::GetAllPatrolPoints()
{
	return PatrolPoints;
}

/**
 * @brief Function that assigns the BK_PlayerInSight in the blackboard
 * @param PlayerInSight 
 */
 void APatrolAIController::GetSightTarget(bool PlayerInSight)
{
	if (Blackboard)
	{
		Blackboard->SetValueAsBool(BK_PlayerInSight, PlayerInSight);
	}
}



