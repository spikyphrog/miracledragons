// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "AICharacter.h"
#include "AIController.h"
#include "AIPatrolPoint.h"
#include "PatrolAIController.generated.h"

UCLASS()
class MIRACLEDRAGONS_API APatrolAIController : public AAIController
{
	GENERATED_BODY()
	
private:
	UPROPERTY()
	class UBehaviorTreeComponent* BehaviourTree; // Declaring a behaviour tree
	
	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess="true"))
	TArray<AActor*> PatrolPoints; // Array for the patrol points that the AI will roam around

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AAIPatrolPoint> PatrolPointClass; // Reference to the Patrol Point class
	
public:
	// Constructor
	APatrolAIController();

	// Called at the start of the game
	virtual void BeginPlay() override;
	
	// BK = Blackboard Key
	UPROPERTY(EditAnywhere)
	FName BK_TargetToFollow = "TargetToFollow";

	UPROPERTY(EditAnywhere)
	FName BK_PlayerInSight = "PlayerInSight";
	
	// Function that overrides the Controller's OnPossess function
	UFUNCTION()
	virtual void OnPossess(APawn* InPawn) override;

	// Function that sets the value of a blackboard key that is passed as an argument in the blackboard
	UFUNCTION()
	void SetSeenTarget(APawn* Target);

	// Function that returns all patrol points
	UFUNCTION()
	TArray<AActor*> GetAllPatrolPoints();

	// Function that sets the value of the blackboard key that is passed as an argument in the blackboard
	UFUNCTION()
	void GetSightTarget(bool PlayerInSight);
};
