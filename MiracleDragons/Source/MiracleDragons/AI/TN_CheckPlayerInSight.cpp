// Hristo Stoyanov 2021


#include "TN_CheckPlayerInSight.h"

#include "AICharacter.h"
#include "PatrolAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MiracleDragons/Character/PlayerCharacter.h"
#include "Perception/PawnSensingComponent.h" 

/**
 * @brief Custom Task that detects if the AI sees the player 
 * @param OwnerComp 
 * @param NodeMemory 
 * @return 
 */
EBTNodeResult::Type UTN_CheckPlayerInSight::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// Reference to the controller
	APatrolAIController* AIController = Cast<APatrolAIController>(OwnerComp.GetAIOwner());

	// Check if the controller is valid
	if (AIController)
	{
		APlayerCharacter* Player = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		AAICharacter* AICharacter = Cast<AAICharacter>(AIController->GetPawn());

		// Check the distance between the player and the sight radius
		if (AICharacter->GetDistanceTo(Player) < AICharacter->PawnSensingComponent->SightRadius)
		{
			// if its less than the radius, then the ai sees the player
			AIController->GetSightTarget(true);
			return EBTNodeResult::Succeeded;
		}
		else
		{
			// if its greater than the radius it does not sees the player
			AIController->GetSightTarget(false);
			return EBTNodeResult::Succeeded;
		}
	}
	return EBTNodeResult::Failed;
}
