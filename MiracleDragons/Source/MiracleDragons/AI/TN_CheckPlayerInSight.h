// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "TN_CheckPlayerInSight.generated.h"

/**
 * 
 */
UCLASS()
class MIRACLEDRAGONS_API UTN_CheckPlayerInSight : public UBTTaskNode
{
	GENERATED_BODY()

	// Overriding a function for custom AI tasks
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
