// Hristo Stoyanov 2021


#include "TN_GetPatrolRoute.h"

#include "AIPatrolPoint.h"
#include "PatrolAIController.h"
#include "BehaviorTree/BlackboardComponent.h" 

/**
 * @brief Function that creates a custom task for the AI 
 * @param OwnerComp 
 * @param NodeMemory 
 * @return 
 */
EBTNodeResult::Type UTN_GetPatrolRoute::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{

	// Creating a reference to the AI controller
	APatrolAIController* AIController = Cast<APatrolAIController>(OwnerComp.GetAIOwner());

	// Check if the controller is valid
	if (AIController)
	{
		UBlackboardComponent* Blackboard = AIController->GetBlackboardComponent(); // Creating a reference to the controller's blackboard
		
		AAIPatrolPoint* CurrentPatrolPoint = Cast<AAIPatrolPoint>(Blackboard->GetValueAsObject("PatrolPoint")); // Creating a reference to the Current Patrol point

		TArray<AActor*> AvailablePatrolPoints = AIController->GetAllPatrolPoints(); // Array of all available patrol points
		
		AAIPatrolPoint* NextPatrolPoint = nullptr; // Creating and initialising the next patrol point

		do
		{
			int32 RandomPatrolPoint = FMath::RandRange(0, AvailablePatrolPoints.Num() - 1);
			NextPatrolPoint = Cast<AAIPatrolPoint>(AvailablePatrolPoints[RandomPatrolPoint]);
		}
		while (CurrentPatrolPoint == NextPatrolPoint);

		Blackboard->SetValueAsObject("PatrolPoint", NextPatrolPoint); // Set the blackboard value of PatrolPoint to the next patrol point when the current patrol point becomes the next one

		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}
