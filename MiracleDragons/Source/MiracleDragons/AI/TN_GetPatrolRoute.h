// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "TN_GetPatrolRoute.generated.h"

/**
 * 
 */
UCLASS()
class MIRACLEDRAGONS_API UTN_GetPatrolRoute : public UBTTaskNode
{
	GENERATED_BODY()
	// Overriding a task function
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
