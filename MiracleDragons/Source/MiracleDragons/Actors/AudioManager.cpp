// Hristo Stoyanov 2021


#include "AudioManager.h"

#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MiracleDragons/GameModes/MainGameMode.h"

// Sets default values
AAudioManager::AAudioManager()
{
}

void AAudioManager::BeginPlay()
{
	Super::BeginPlay();
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
}

/**
 * @brief Function that returns a sound cue on the passed index
 * @param AudioIndex 
 * @return 
 */
USoundCue* AAudioManager::GetSound(int32 AudioIndex)
{
	if (AudioFiles.Num() != 0)
	{
		return AudioFiles[AudioIndex];
	}
	return nullptr;
}

USoundCue* AAudioManager::GetRandomRespawnCubeSound()
{
	if (RespawnCubesAudioFiles.Num() != 0)
	{
		return RespawnCubesAudioFiles[FMath::RandRange(0, RespawnCubesAudioFiles.Num() - 1)];
	}
	return nullptr;
}

USoundCue* AAudioManager::GetRandomRespawnBallSound()
{
	if (RespawnBallsAudioFiles.Num() != 0)
	{
		return RespawnBallsAudioFiles[FMath::RandRange(0, RespawnBallsAudioFiles.Num() - 1)];
	}
	return nullptr;
}

USoundCue* AAudioManager::GetRandomCaughtByAISound()
{
	if (CaughtByAIAudioFiles.Num() != 0)
	{
		return CaughtByAIAudioFiles[FMath::RandRange(0, CaughtByAIAudioFiles.Num() - 1)];
	}
	return nullptr;
}

UAudioComponent* AAudioManager::PlaySound(USoundCue* SoundToPlay)
{
	UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(GetWorld(), SoundToPlay, 1.5f);

	AudioComponent->Play();
	
	AudioComponent->OnAudioFinished.AddDynamic(this, &AAudioManager::ResetGameModeIsPlaying);

	GameMode->CurrentlyPlayingSound = AudioComponent;
	GameMode->bIsPlaying = true;
	return AudioComponent;
}

void AAudioManager::ResetGameModeIsPlaying()
{
	GameMode->bIsPlaying = false;
	GameMode->CurrentlyPlayingSound = nullptr;
}

bool AAudioManager::CheckIfNeedHelp()
{
	if(GameMode->ObjectivesManager && GameMode->ObjectivesManager->bHasCompletedZone1_A)
	{
		return false;
	}
	
	return true;
}


