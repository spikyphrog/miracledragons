// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"
#include "AudioManager.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AAudioManager : public AActor
{
	GENERATED_BODY()
	
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess="true"))
	TArray<USoundCue*> AudioFiles; // Array to store all the sound cues
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess="true"))
	TArray<USoundCue*> RespawnBallsAudioFiles; // Array to store all the sound cues
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess="true"))
	TArray<USoundCue*> RespawnCubesAudioFiles; // Array to store all the sound cues

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<USoundCue*> CaughtByAIAudioFiles;

	UPROPERTY(VisibleAnywhere)
	class AMainGameMode* GameMode;

public:	
	// Sets default values for this actor's properties
	AAudioManager(); // Constructor

protected:
	virtual void BeginPlay() override;

public:
	// Function that will get a sound by the given index
	UFUNCTION(BlueprintPure)
	USoundCue* GetSound(int32 AudioIndex);
	// Function that will get a sound by the given index
	
	UFUNCTION(BlueprintPure)
	USoundCue* GetRandomRespawnCubeSound();
	
	// Function that will get a sound by the given index
	UFUNCTION(BlueprintPure)
	USoundCue* GetRandomRespawnBallSound();

	UFUNCTION(BlueprintPure)
	USoundCue* GetRandomCaughtByAISound();
	
	UFUNCTION(BlueprintCallable)
	UAudioComponent* PlaySound(USoundCue* SoundToPlay);

	UFUNCTION()
	void ResetGameModeIsPlaying();

	UFUNCTION(BlueprintCallable)
	bool CheckIfNeedHelp();
};
