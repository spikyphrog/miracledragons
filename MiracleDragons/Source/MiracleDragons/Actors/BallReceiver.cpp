// Hristo Stoyanov 2021


#include "BallReceiver.h"

// Sets default values
ABallReceiver::ABallReceiver()
{
	// Creating the mesh for the receivers. Redundant, and could have been a blueprint, due to unsuccessful integration of the Receiver Trigger within the Ball
	// Receiver, this was left as a standalone class.
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh"));
}
