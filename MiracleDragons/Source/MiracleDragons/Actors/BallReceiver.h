// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BallReceiver.generated.h"

UCLASS()
class MIRACLEDRAGONS_API ABallReceiver : public AActor
{
	GENERATED_BODY()
	
	// Variables
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh; // The static mesh that represents the trigger

	// Constructor 	
public:	
	// Sets default values for this actor's properties
	ABallReceiver();

	
};
