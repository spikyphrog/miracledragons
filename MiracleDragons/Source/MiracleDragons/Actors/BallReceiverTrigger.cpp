// Hristo Stoyanov 2021

#include "BallReceiverTrigger.h"

#include "AudioManager.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "InteractableBallSpawner.h"
#include "MiracleDragons/Components/Ball.h"
#include "MiracleDragons/GameModes/MainGameMode.h"

// Sets default values
ABallReceiverTrigger::ABallReceiverTrigger()
{
	// Creating the box component
	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collider"));

	// Binding the OnComponentBegin and End Overlap events
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &ABallReceiverTrigger::OnOverlapBegin);
	BoxCollider->OnComponentEndOverlap.AddUniqueDynamic(this, &ABallReceiverTrigger::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ABallReceiverTrigger::BeginPlay()
{
	// Calling the super class
	Super::BeginPlay();

	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());

	// Storing a reference to the ball spawner in the level (can not have more than one in the level) 
	BallSpawner = Cast<AInteractableBallSpawner>(UGameplayStatics::GetActorOfClass(GetWorld(), BallSpawnerClass));

	// Storing a reference to the Level Stream Manger in the level (can not have more than one in the level) 
	LevelStreamManager = Cast<ALevelStreamManager>(UGameplayStatics::GetActorOfClass(GetWorld(), LevelStreamManagerClass));

}

/// Function that is bound to the OnComponentBeginOverlap
void ABallReceiverTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Creating a reference to the Ball Component Class, then casting it from the overlapped actor, to see if it implements that component
	UBall* Ball = Cast<UBall>(OtherActor->GetComponentByClass(UBall::StaticClass()));

	// Check if Ball is valid and not nullptr and if the ball is grabbed
	if (Ball && !Ball->IsGrabbed)
	{
		// Check to see if the player has started the word Ale and has Not completed the word Miracle
		// OR if the player has completed Ale and Not started Miracle
		if (!GameMode->bHasStartedAle && !GameMode->bHasStartedMiracle || GameMode->bHasCompletedAle && !GameMode->bHasStartedMiracle)
		{
			// Call the function to check what word the player has started
			GameMode->CheckWord(Ball->DefaultLetter);
		}

		// 
		if (!bHasAlreadyLoadedChallenge)
		{
			// Check if the player has started Miracle and the ball matches the required letter and the player has Not completed Miracle
			if (GameMode->bHasStartedMiracle && Ball->DefaultLetter == LetterRequired && !GameMode->bHasCompletedMiracle)
			{
				GameMode->IsLevelCompleted = true;

				// Add the letter to the array of input letters
				GameMode->PlayerInputWord.Add(Ball->DefaultLetter);

				// Setting a control variable within the trigger to check if the trigger has loaded a challenge
				bHasAlreadyLoadedChallenge = true;
				
				// Checks if the word is complete
				if (GameMode->CheckIfPlayerWordMatchMiracle() && !GameMode->bHasCompletedMiracle)
				{
					// Play sound when the word is completed, to let the player know that they are done with it
					GameMode->AudioManager->PlaySound(GameMode->AudioManager->GetSound(0));

					// Setting a bool, that indicates the player has completed the word Miracle 
					GameMode->bHasCompletedMiracle = true;
				}

				// Check if the player has Not completed the word Miracle
				if (!GameMode->bHasCompletedMiracle)
				{
					// Spawn the next level, this function takes an argument of AActor to destroy after it spawns the next level
					SpawnLevel(OtherActor);
				}
				else
				{
					// Shrink the ball in order to trigger on Overlap End and to unload the level
					Shrink(OtherActor);
				}

				// Set a control bool that will be used when checking if the word completed is Ale
				bIsTheRightLetter = true;

				// Setting a bool trigger when the ball has the correct letter, to allow the trigger to shrink the ball
				// Shrinking is necessary in order to call the OnOverlapEnd
				bHasPassedToShrink = true;
			}

		// Check if the player has started the word Ale and the ball matches the required optional letter and the player has Not completed the word Ale
		// IMPORTANT TO CHECK IF THE WORD THAT HAS BEEN STARTED IS NOT COMPLETED!
		else if(GameMode->bHasStartedAle && Ball->DefaultLetter == OptionalLetter && !GameMode->bHasCompletedAle)
		{
			// 
			GameMode->IsLevelCompleted = true;

			// Add the letter to the input array
			GameMode->PlayerInputWord.Add(Ball->DefaultLetter);

			// Setting a control variable within the trigger to check if the trigger has loaded a challenge
			bHasAlreadyLoadedChallenge = true;

			// Check if the word that the player has input matches the optional word and if the player has not completed Ale before
			if (GameMode->CheckIfPlayerWordMatchOptional() && !GameMode->bHasCompletedAle)
			{
				GameMode->AudioManager->PlaySound(GameMode->AudioManager->GetSound(1)); 
				// Setting a bool, that indicates the player has completed the word Ale 
				GameMode->bHasCompletedAle = true;
			}

			// Check if the player has Not completed the word Ale
			if (!GameMode->bHasCompletedAle)
			{
				// Spawn the next challenge
				SpawnLevel(OtherActor);
			}

			// Set a control bool that will be used when checking if the word completed is Ale
			bIsTheRightLetter = true;
			
			// Setting a bool trigger when the ball has the correct letter, to allow the trigger to shrink the ball
			bHasPassedToShrink = true;
			
			// Shrinking the overlapped actor, this is called here to prevent from the ball being stuck after the player
			// completes the word Ale. Shrinking is necessary in order to call the OnOverlapEnd
			Shrink(OtherActor);
		}
		// If the ball's letter does not match the required nor the optional letter
		else
		{
			// Check if we have a valid Ball Spawner
			if (BallSpawner)
			{
				// Respawn the ball
				BallSpawner->Respawn();

				// Destroy the ball, to allow the trigger to receive other attempts
				OtherActor->Destroy();

				// Set a control bool that will be used when checking if the word completed is Ale
				bIsTheRightLetter = false;
			}

			// Preventing from the ball with wrong letter to shrink as it is not necessary
			bHasPassedToShrink = false;
		}

			// Check if the player has completed Ale and has Not started Miracle, and the control bool for the correct letter is true
			if (GameMode->bHasCompletedAle && !GameMode->bHasStartedMiracle && bIsTheRightLetter)
			{
				// Setting a control variable within the trigger to check if the trigger has loaded a challenge
				bHasAlreadyLoadedChallenge = true;

				// Clear the input array, when the player completes Ale to allow the player to start inputting Miracle
				GameMode->PlayerInputWord.Empty();

				// Reset the receivers
				GameMode->RestartBallReceivers();
			}
		}
	
	}
}

/// Function that is bound to the OnComponentEndOverlap 
void ABallReceiverTrigger::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Check if the trigger is allowed to shrink the ball
	if (bHasPassedToShrink)
	{
		// Check to see if the ball has already landed on the ground of the receiver
		if (!bHasAlreadySink)
		{
			// Check if there is a valid LevelStreamManager, GameMode and the trigger has already loaded a challenge
			if (LevelStreamManager && GameMode && bHasAlreadyLoadedChallenge)
			{
				
				if (GameMode->bHasCompletedMiracle)
				{
					GameMode->UnloadLevel(LevelStreamManager->LevelNames[GameMode->CurrentLevelIndex-1]);
				}

				// Unload the current level
				GameMode->UnloadLevel(LevelStreamManager->LevelNames[GameMode-> CurrentLevelIndex-2]);

				// Destroy the ball, because it is spawned in the persistent level and will remain in the game even
				// if the challenge gets unloaded
				OtherActor->Destroy();				
			}

			// The ball has hit the ground of the receiver
			bHasAlreadySink = true;
		}
	}
}

/**
 * @brief - Function that handles the shrinking of the balls
 * @param Actor - The ball to shrink
 */
 void ABallReceiverTrigger::Shrink(AActor* Actor) const
{
	// Get the current scale of the ball
	FVector CurrentScale = Actor->GetActorScale3D();

	// Lerp all axis from their current scale to 0.1
	CurrentScale.X = FMath::Lerp(CurrentScale.X, .1f, 1.f);
	CurrentScale.Y = FMath::Lerp(CurrentScale.Y, .1f, 1.f);
	CurrentScale.Z = FMath::Lerp(CurrentScale.Z, .1f, 1.f);

	// Set the actor new scale
	Actor->SetActorScale3D(CurrentScale);
}

/**
 * @brief Function that handles Loading Challenges
 * @param OtherActor Ball to shrink
 */
void ABallReceiverTrigger::SpawnLevel(AActor* OtherActor) 
{
	// Check if there is a valid LevelStreamManager and if the level is completed
	if (LevelStreamManager && GameMode->IsLevelCompleted)
	{
		// Load the next challenge
		GameMode->LoadLevel(LevelStreamManager->LevelNames[GameMode->CurrentLevelIndex]);

		// Shrink the ball
		Shrink(OtherActor);
	}
}

