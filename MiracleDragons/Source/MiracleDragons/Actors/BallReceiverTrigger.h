// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "LevelStreamManager.h"
#include "GameFramework/Actor.h"
#include "BallReceiverTrigger.generated.h"

// Predefining classes, which will be included in the implementation file
class AInteractableBallSpawner;

UCLASS()
class MIRACLEDRAGONS_API ABallReceiverTrigger : public AActor
{
	GENERATED_BODY()
	// Variables
public:
	UPROPERTY(EditDefaultsOnly)
	class UBoxComponent* BoxCollider; // Collider for the trigger
	
	UPROPERTY(EditAnywhere)
	FString LetterRequired; // Property for the required word (Miracle)

	UPROPERTY(EditAnywhere)
	FString OptionalLetter; // Property for the optional word (Ale)

	UPROPERTY(VisibleAnywhere)
	AInteractableBallSpawner* BallSpawner; // Reference to the ball spawner
	
private:
	UPROPERTY(VisibleAnywhere)
	class AMainGameMode* GameMode; // Reference to the custom game mode

	UPROPERTY(VisibleAnywhere)
	ALevelStreamManager* LevelStreamManager; // Reference to the Level Stream Manager
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AInteractableBallSpawner> BallSpawnerClass; // Reference to the BallSpawner class
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ALevelStreamManager> LevelStreamManagerClass; // Reference to the LevelStreamManager class
	
	UPROPERTY(VisibleAnywhere)
	bool bHasAlreadyLoadedChallenge = false; // Property if the trigger has loaded a challenge

	UPROPERTY(VisibleAnywhere)
	bool bHasPassedToShrink = false; // Property to check if the ball is allowed to shrink

	UPROPERTY(VisibleAnywhere)
	bool bHasAlreadySink = false; // Property to check if the ball has already sink

	UPROPERTY(VisibleAnywhere)
	bool bIsTheRightLetter = false; // Property to check if the ball's letter is the correct one
	
	// Constructor
public:	
	// Sets default values for this actor's properties
	ABallReceiverTrigger();

	// Functions
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Function that is bound to the OnComponentBeginOverlap. Must be marked as UFUNCTION!
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Function that is bound to the OnComponentEndOverlap. Must be marked as UFUNCTION!
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void Shrink(AActor* Actor) const; // Function that will handle the shrinking of the balls

	UFUNCTION()
	void SpawnLevel(AActor* OtherActor); // Function that loads challenges.


};
