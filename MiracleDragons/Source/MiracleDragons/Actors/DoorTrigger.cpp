// Hristo Stoyanov 2021


#include "DoorTrigger.h"

#include "MiracleDragons/Actors/ObjectivesManager.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ADoorTrigger::ADoorTrigger()
{
	// Creating the box collider
	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));

	// Binding the Begin and End overlap events
	OnActorBeginOverlap.AddDynamic(this, &ADoorTrigger::OnBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &ADoorTrigger::OnEndOverlap);
}


// Called when the game starts or when spawned
void ADoorTrigger::BeginPlay()
{
	Super::BeginPlay();

	// Setting the property that will be later used to check if there is an actor overlapping the trigger
	bHasActorOverlapping = false;

	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());

	// Storing a reference to the Level Stream and Objectives manager
	LevelStreamManager = Cast<ALevelStreamManager>(UGameplayStatics::GetActorOfClass(GetWorld(), LevelStreamManagerClass));
	ObjectivesManager = Cast<AObjectivesManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ObjectivesManagerClass));
} 

/**
 * @brief Function that is bound to the OnActorBeginOverlap
 * @param OverlappedActor 
 * @param OtherActor 
 */
void ADoorTrigger::OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	// Check to see if there is valid other actor and if the actor is not this trigger
	if (OtherActor && OtherActor != this)
	{
		// Add the overlapped actor to the array of OverlappedActors
		OverlappedActors.Add(OtherActor);

		// Set the control property to true
		bHasActorOverlapping = true;

		// Check if there is valid game mode and level manager
		if (GameMode && LevelStreamManager)
		{
			// And check if the door trigger has not already loaded a level
			if (!GameMode->bHasDoorTriggerAlreadyLoadedLevel)
			{
				// Then load the first level in the next zone
				GameMode->LoadLevel(LevelStreamManager->LevelNames[GameMode->CurrentLevelIndex]);

				// Set the property to true to prevent this statement from ever triggering again
				GameMode->bHasDoorTriggerAlreadyLoadedLevel = true;
			}
		}
	}
}

/**
 * @brief Function that is bound to the OnActorEndOverlap
 * @param OverlappedActor 
 * @param OtherActor 
 */
 void ADoorTrigger::OnEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	// Check if there is a valid other actor and its not this trigger
	if (OtherActor && OtherActor != this)
	{
		// Remove the actors that left the trigger
		OverlappedActors.Remove(OtherActor);

		// Check if the OverlappedActors array is empty, then set the has actor overlapping to false
		if(OverlappedActors.Num() == 0)	{ bHasActorOverlapping = false; }
	}
}

/**
 * @brief A function that returns the current state of the boolean HasActorOverlapping 
 * @return bHasActorOverlapping
 */
 bool ADoorTrigger::IsOverlapping() const
{
	return bHasActorOverlapping;
}
