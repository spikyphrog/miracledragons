// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"

#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "MiracleDragons/GameModes/MainGameMode.h"
#include "DoorTrigger.generated.h"

// Predefining the classes, to save compile time
class ULevelStreamComponent;
class AObjectivesManager;

UCLASS()
class MIRACLEDRAGONS_API ADoorTrigger : public AActor
{
	GENERATED_BODY()
	
	// Variables
private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* BoxCollider; // Property for the box trigger
	
	UPROPERTY(VisibleAnywhere)
	bool bHasActorOverlapping; // Property to check if there are active overlapping actors in the trigger
	
	UPROPERTY(VisibleAnywhere)
	AObjectivesManager* ObjectivesManager; // Reference to the Objectives Manager
	
	UPROPERTY(VisibleAnywhere)
	ALevelStreamManager* LevelStreamManager; // Reference to the LevelStreamManager
	
	UPROPERTY(VisibleAnywhere)
	AMainGameMode* GameMode; // Reference to the custom game mode
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObjectivesManager> ObjectivesManagerClass; // Reference to the ObjectivesManagerClass
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ALevelStreamManager> LevelStreamManagerClass; // Reference to the LevelStreamManagerClass
	
	UPROPERTY()
	TArray<AActor*> OverlappedActors; // Array for all the actors that has overlapped the trigger


public:	
	// Constructor
	ADoorTrigger();

	// Functions
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Function that is bound to the OnActorBeginOverlap
	UFUNCTION()
	void OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	
	// Function that is bound to the OnActorEndOverlap
	UFUNCTION()
	void OnEndOverlap(AActor* OverlappedActor, AActor* OtherActor);
	
public:
	UFUNCTION()
	bool IsOverlapping() const; // Function that returns the current state of the bool bHasActorOverlapping
};
