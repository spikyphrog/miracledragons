// Hristo Stoyanov 2021


#include "DragonCollectable.h"
#include "MiracleDragons/GameModes/MainGameMode.h"

// Sets default values
ADragonCollectable::ADragonCollectable()
{
	// Creating the static mesh of the dragon
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	// Setting the root component to be the static mesh
	RootComponent = StaticMesh;
}

// Called when the game starts or when spawned
void ADragonCollectable::BeginPlay()
{
	Super::BeginPlay();

	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
}

/**
 * @brief Interface function, for grabbing interactable objects
 * @param ActorHit Object to grab
 * @param SceneComponentToAttachTo Component to attach the object 
 */
 void ADragonCollectable::GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo)
{
	// Check if there is a valid Game mode
	if (GameMode)
	{
		// Increase the CollectedDragons count
		GameMode->CollectedDragons++;

		// Check if there is a valid object 
		if (ActorHit)
		{
			// Destroy the actor
			ActorHit->Destroy();
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("GameMode not found!"));
	}
}

/**
 * @brief Interface Function to release the object. Empty due to not grabbing any objects 
 * @param SceneComponentToDetachFrom 
 */
 void ADragonCollectable::ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom)
{
	
}

/**
 * @brief Interface Function to get the object's information
 * @return The information
 */
 FString ADragonCollectable::InteractableInfo()
{
	return "Dragon";
}

/**
 * @brief Interface Function to set time to disappear the information text
 * @return 
 */
 float ADragonCollectable::GetTimeToDisappearInfo()
{
	return 3.f;
}

/**
 * @brief Interface function that clears the information text
 * @return 
 */
 FString ADragonCollectable::ClearInteractableInfo()
{
	return "";
}

