// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MiracleDragons/Interfaces/Interactable.h"
#include "DragonCollectable.generated.h"

UCLASS()
class MIRACLEDRAGONS_API ADragonCollectable : public AActor, public IInteractable
{
	GENERATED_BODY()

	// Variables
private:
	UPROPERTY()
	class AMainGameMode* GameMode; // Reference to the custom Game Mode

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh; // Static mesh of the dragon


public:	
	// Constructor
	ADragonCollectable();
	
	// Functions
	
	// Interactable Interface Functions
	virtual void GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo) override;

	virtual void ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom) override;

	virtual FString InteractableInfo() override;

	virtual float GetTimeToDisappearInfo() override;

	virtual FString ClearInteractableInfo() override;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
