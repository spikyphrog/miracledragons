// Hristo Stoyanov 2021


#include "InteractableBall.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MiracleDragons/Components/Ball.h"
#include "MiracleDragons/GameModes/MainGameMode.h"


// Sets default values
AInteractableBall::AInteractableBall()
{
	// Creating the mesh of the ball
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetSimulatePhysics(true); // Setting the ball's physics to true once spawned
	RootComponent = StaticMesh;	// Setting the root component to the ball's static mesh
	
	BallComp = CreateDefaultSubobject<UBall>(TEXT("Ball component")); // Creating ball component
	
	ItemInfo = "Interactable ball. "; // Setting the ball's info
}

// Called when the game starts or when spawned
void AInteractableBall::BeginPlay()
{
	Super::BeginPlay();

	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
}

/**
 * @brief Interface function, for grabbing interactable objects
 * @param ActorHit Object to grab
 * @param SceneComponentToAttachTo Component to attach the object 
 */
void AInteractableBall::GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo)
{
	const FAttachmentTransformRules AttachmentRules (EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true);
	const FVector RelativeLocation(0.f, 0.f, 0.f);
	const FLatentActionInfo LatentActionInfo; 
	const FVector ShrinkScale(.5f, .5f, .5f);	// Scale to shrink to
	
	// Attaching the static mesh to the passed component
	StaticMesh->AttachToComponent(SceneComponentToAttachTo, AttachmentRules, NAME_None);

	// Moving the grabbed object
	UKismetSystemLibrary::MoveComponentTo(ActorHit->GetRootComponent(), RelativeLocation, ActorHit->GetActorRotation(),
		false, false, .2f,false, EMoveComponentAction::Move, LatentActionInfo);

	// Setting the grabbed object's transform to the component to attach position
	StaticMesh->SetWorldTransform(FTransform(SceneComponentToAttachTo->GetComponentRotation(), SceneComponentToAttachTo->GetComponentLocation(), ShrinkScale),false, nullptr, ETeleportType::None);

	// Check if it is simulating physics
	if (StaticMesh->IsSimulatingPhysics())
	{
		// Set the physics to false
		StaticMesh->SetSimulatePhysics(false);
	}

	// Check if there is valid ball component
	if (BallComp)
	{
		// Set the ball's ball component is grabbed to true
		BallComp->IsGrabbed = true;
	}
	
	// Set the position of the grabbed object
	SetActorLocation(SceneComponentToAttachTo->GetComponentLocation(),false, nullptr, ETeleportType::None);
}

/**
 * @brief Interface Function to release the object.
 * @param SceneComponentToDetachFrom 
 */
void AInteractableBall::ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom)
{
	// Rules needed for the DetachFromActor function
	const FDetachmentTransformRules DetachmentTransformRules(EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld, false);
	const FVector ReScale(1.f, 1.f, 1.f); // 

	// Detaching the grabbed actor
	DetachFromActor(DetachmentTransformRules);

	// Setting the relative transformation of the ball to release
	StaticMesh->SetRelativeTransform(FTransform(SceneComponentToDetachFrom->GetComponentRotation(), SceneComponentToDetachFrom->GetComponentLocation(), ReScale),false, nullptr, ETeleportType::None);

	// Set the physics of ball to true
	StaticMesh->SetSimulatePhysics(true);

	// Check if there is a valid ball component
	if (BallComp)
	{
		// set the is grabbed to false
		BallComp->IsGrabbed = false;
	}
}

/**
 * @brief Interface Function to get the object's information
 * @return The information
 */
FString AInteractableBall::InteractableInfo()
{
	// Check if there is a valid ball component
	if (BallComp)
	{
		FString BallLetter = "This ball contains the letter: ";
		BallLetter += BallComp->DefaultLetter;

		return ItemInfo += BallLetter;
	}
	return ItemInfo;
}

/**
 * @brief Interface Function to set time to disappear the information text
 * @return 
 */
float AInteractableBall::GetTimeToDisappearInfo()
{
	return 5.f;
}

/**
 * @brief Interface function that clears the information text
 * @return 
 */
FString AInteractableBall::ClearInteractableInfo()
{
	return ItemInfo = "";
}


