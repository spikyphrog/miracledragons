// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MiracleDragons/Interfaces/Interactable.h"
#include "InteractableBall.generated.h"


UCLASS()
class MIRACLEDRAGONS_API AInteractableBall : public AActor, public IInteractable
{
	GENERATED_BODY()
	
	// Variables
private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh; // Ball static mesh
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UBall* BallComp; // Ball component

	UPROPERTY(VisibleAnywhere)
	class AMainGameMode* GameMode; // Reference to the custom game mode
	
	UPROPERTY(VisibleAnywhere)
	FString ItemInfo; // Property for the ball information

	// Constructor
public:	
	// Sets default values for this actor's properties
	AInteractableBall();

	// Functions
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// == Interactable Interface == 
	virtual void GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo) override;
	
	virtual void ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom) override;
	
	virtual FString InteractableInfo() override;
	
	virtual float GetTimeToDisappearInfo() override;

	virtual FString ClearInteractableInfo() override;
};
