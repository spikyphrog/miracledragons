// Hristo Stoyanov 2021


#include "InteractableBallSpawner.h"

#include "AudioManager.h"
#include "MiracleDragons/Actors/InteractableBall.h"
#include "MiracleDragons/GameModes/MainGameMode.h"

// Sets default values
AInteractableBallSpawner::AInteractableBallSpawner()
{
}

// Called when the game starts or when spawned
void AInteractableBallSpawner::BeginPlay()
{
	Super::BeginPlay();
	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());

	// Check if the the spawner is allowed to spawn balls
	if (bAllowToSpawnBalls)
	{
		// Create a timer, using the timer handle, that would spawn a ball every SpawnRate and will loop
		GetWorldTimerManager().SetTimer(BallSpawningHandle,this, &AInteractableBallSpawner::InitSpawn, SpawnRate, true);
	}
	
}

/**
 * @brief Function that handles Respawning
 */
 void AInteractableBallSpawner::Respawn()
{
	GetWorld()->SpawnActor<AActor>(InteractableBallClass, GetActorLocation(), GetActorRotation());
	
	if (!GameMode->bIsPlaying && GameMode->CurrentlyPlayingSound == nullptr)
	{
		GameMode->AudioManager->PlaySound(GameMode->AudioManager->GetRandomRespawnBallSound());
	}
}

/**
 * @brief Function bound to the timer that handles the first spawning of the balls
 */
 void AInteractableBallSpawner::InitSpawn()
{
	if (bSpawnLevel7 && AmountBallsSpawned != 3)
	{
		GetWorld()->SpawnActor<AActor>(InteractableBallClass, GetActorLocation(), GetActorRotation()); // Spawn a ball
		AmountBallsSpawned ++; // Increase the counter
	}
	else if (!bSpawnLevel7 && AmountBallsSpawned != GameMode->MiracleWord.Num() + GameMode->OptionalWord.Num()) 	// Check if the amounts balls spawned are not equal to the both letter's lenght 
	{
		GetWorld()->SpawnActor<AActor>(InteractableBallClass, GetActorLocation(), GetActorRotation()); // Spawn a ball
		AmountBallsSpawned ++; // Increase the counter
	}
	// If it reaches the limit
	else
	{
		// Clear the timer by destroying the timer handle
		GetWorldTimerManager().ClearTimer(BallSpawningHandle);
	}	
}
