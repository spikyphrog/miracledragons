// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableBallSpawner.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AInteractableBallSpawner : public AActor
{
	GENERATED_BODY()
	// Variables
private:
	// Control property for spawning balls. Used in the level streaming challenges, where a level would have a spawner to enable respawnabilty
	UPROPERTY(EditAnywhere)
	bool bAllowToSpawnBalls;

	UPROPERTY(EditAnywhere)
	bool bSpawnLevel7;
	
	UPROPERTY(EditDefaultsOnly)
	float SpawnRate = 3.f; // Property for the speed of spawning
	
	UPROPERTY(VisibleAnywhere)
	int32 AmountBallsSpawned; // Property for the amount balls spawned
	
	UPROPERTY(VisibleAnywhere)
	class AMainGameMode* GameMode; // Reference to the custom game mode 
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AInteractableBall> InteractableBallClass; //Reference to the Interactable ball class

	UPROPERTY()
	FTimerHandle BallSpawningHandle; // Timer handle for the Ball Spawning timer

	// Constructor
public:	
	// Sets default values for this actor's properties
	AInteractableBallSpawner();

	// Functions
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	//== Respawning functionality ==
	void InitSpawn(); // Function that handles initial spawning

	void Respawn(); // Function that handles respawning
};
