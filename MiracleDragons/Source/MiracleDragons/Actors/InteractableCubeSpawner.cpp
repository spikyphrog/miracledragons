// Hristo Stoyanov 2021


#include "InteractableCubeSpawner.h"

#include "AudioManager.h"
#include "InteractableObject.h"
#include "MiracleDragons/GameModes/MainGameMode.h"

// Sets default values
AInteractableCubeSpawner::AInteractableCubeSpawner()
{
}

// Called when the game starts or when spawned
void AInteractableCubeSpawner::BeginPlay()
{
	Super::BeginPlay();

	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
	
	// Creating a timer to spawn the cubes when the spawner is initialised
	GetWorldTimerManager().SetTimer(SpawnCubeTimerHandle, this, &AInteractableCubeSpawner::SpawnCube, SpawnDelay, true);
}

/**
 * @brief Function that handles the initial spawning
 */
 void AInteractableCubeSpawner::SpawnCube()
{
	// Check if it can increase it's cap
	if (bCanIncreaseCap) SpawnCap = 10; // Increase the cap 

	// Check if the currently spawned cubes are less than the cap
	if (CurrentSpawn < SpawnCap)
	{
		// Spawn the cubes
		const FVector Location = GetActorLocation();
		const FRotator Rotation = GetActorRotation();
		GetWorld()->SpawnActor<AActor>(InteractableCubeClass, Location, Rotation);
		CurrentSpawn++; // Increase the current spawn
	}
	// If it exceeds the cap
	else
	{
		// Clear the timer
		GetWorldTimerManager().ClearTimer(SpawnCubeTimerHandle);
	}
}

/**
 * @brief Function that handles the respawning
 */
 void AInteractableCubeSpawner::RespawnCube() const
{
	GetWorld()->SpawnActor<AActor>(InteractableCubeClass,  GetActorLocation(), GetActorRotation()); // Spawn the cube
	if (!GameMode->bIsPlaying && GameMode->CurrentlyPlayingSound == nullptr)
	{
		GameMode->AudioManager->PlaySound(GameMode->AudioManager->GetRandomRespawnCubeSound());
	}
}


