// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableCubeSpawner.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AInteractableCubeSpawner : public AActor
{
	GENERATED_BODY()
	// Variables
private:
	UPROPERTY(EditAnywhere)
	float SpawnDelay = 4.f; // Property for the delay between spawning a cube
	
	UPROPERTY(EditAnywhere)
	int32 SpawnCap = 5; // Property for the maximum amount of spawned cubes per spawner

	UPROPERTY()
	int32 CurrentSpawn = 0; // Property for the amount of currently spawned cubes

	UPROPERTY(EditAnywhere)
	bool bCanIncreaseCap = false; // Property for the option to increase the cap of the spawner

	UPROPERTY(VisibleAnywhere)
	class AMainGameMode* GameMode; // Reference to the custom game mode
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<class AInteractableObject> InteractableCubeClass; // Reference to the Interactable Cube Class
	
	UPROPERTY()
	FTimerHandle SpawnCubeTimerHandle; // Timer handle for the cube spawning

	// Constructor
	public:	
	// Sets default values for this actor's properties
	AInteractableCubeSpawner();

	// Functions
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//== Respawning functionality ==
public:
	void SpawnCube(); // Function that handles initial spawning

	void RespawnCube() const; // Function that handles respawning
};
