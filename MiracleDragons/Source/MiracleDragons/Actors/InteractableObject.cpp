// Hristo Stoyanov 2021


#include "InteractableObject.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
AInteractableObject::AInteractableObject()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh")); // Create the static mesh of the object
	RootComponent = StaticMesh; // Set the root component to be the static mesh
}

/**
 * @brief Interface function, for grabbing interactable objects
 * @param ActorHit Object to grab
 * @param SceneComponentToAttachTo Component to attach the object 
 */
void AInteractableObject::GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo)
{
	const FAttachmentTransformRules AttachmentRules (EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true);
	const FVector RelativeLocation(0.f, 0.f, 0.f);
	const FLatentActionInfo LatentActionInfo;
	const FVector ShrinkScale(.5f, .5f, .5f);
	
	// Attach the grabbed object to the component holder
	StaticMesh->AttachToComponent(SceneComponentToAttachTo, AttachmentRules, NAME_None);

	// Move the object
	UKismetSystemLibrary::MoveComponentTo(ActorHit->GetRootComponent(), RelativeLocation, ActorHit->GetActorRotation(),
		false, false, .2f,false, EMoveComponentAction::Move, LatentActionInfo);

	// Set the transform of the grabbed object to the scene component's transform
	StaticMesh->SetWorldTransform(FTransform(SceneComponentToAttachTo->GetComponentRotation(), SceneComponentToAttachTo->GetComponentLocation(), ShrinkScale),false, nullptr, ETeleportType::None);

	// Check if the grabbed object is simulating physics
	if (StaticMesh->IsSimulatingPhysics())
	{
		// Disable physics
		StaticMesh->SetSimulatePhysics(false);
	}

	// Set the location of the grabbed object
	SetActorLocation(SceneComponentToAttachTo->GetComponentLocation(),false, nullptr, ETeleportType::None);
}

/**
 * @brief Interface Function to release the object.
 * @param SceneComponentToDetachFrom 
 */
void AInteractableObject::ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom)
{
	const FDetachmentTransformRules DetachmentTransformRules(EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld, false);
	const FVector Upscale(1.f, 1.f, 1.f);

	// Detaching the grabbed object
	DetachFromActor(DetachmentTransformRules);

	// Setting the detached object's transform
	StaticMesh->SetRelativeTransform(FTransform(SceneComponentToDetachFrom->GetComponentRotation(), SceneComponentToDetachFrom->GetComponentLocation(), Upscale),false, nullptr, ETeleportType::None);

	// Enabling the physics
	StaticMesh->SetSimulatePhysics(true);	
}

/**
 * @brief Interface Function to get the object's information
 * @return The information
 */
FString AInteractableObject::InteractableInfo()
{
	return "This is interactable cube.";
}

/**
 * @brief Interface Function to set time to disappear the information text
 * @return 
 */
float AInteractableObject::GetTimeToDisappearInfo()
{
	return 3.f;
}

/**
 * @brief Interface function that clears the information text
 * @return 
 */
FString AInteractableObject::ClearInteractableInfo()
{
	return "";
}
