// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MiracleDragons/Interfaces/Interactable.h"
#include "InteractableObject.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AInteractableObject : public AActor, public IInteractable
{
	GENERATED_BODY()
	// Variables
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh; // Mesh of the interactable object
	
public:	
	// Sets default values for this actor's properties
	AInteractableObject(); // Constructor

	// Interface Functions
	virtual void GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo) override; // The C++ function

	virtual void ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom) override;

	virtual FString InteractableInfo() override;

	virtual float GetTimeToDisappearInfo() override;

	virtual FString ClearInteractableInfo() override;
	
};
