// Hristo Stoyanov 2021


#include "InteractableStaticObject.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AInteractableStaticObject::AInteractableStaticObject()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh")); // Create the static mesh
	StaticMesh->SetSimulatePhysics(true); // Enable the physics to true
	RootComponent = StaticMesh; // Set the root component to be the mesh
}

/**
 * @brief Interface function, for grabbing interactable objects
 * @param ActorHit Object to grab
 * @param SceneComponentToAttachTo Component to attach the object 
 */
void AInteractableStaticObject::GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo)
{
	const FAttachmentTransformRules AttachmentRules (EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true);
	const FVector RelativeLocation(0.f, 0.f, 0.f);
	const FLatentActionInfo LatentActionInfo;
	const FVector ShrinkScale(.5f, .5f, .5f);

	// Attach the grabbed object to the scene component
	StaticMesh->AttachToComponent(SceneComponentToAttachTo, AttachmentRules, NAME_None);

	// Move the object
	UKismetSystemLibrary::MoveComponentTo(ActorHit->GetRootComponent(), RelativeLocation, ActorHit->GetActorRotation(),
		false, false, .2f,false, EMoveComponentAction::Move, LatentActionInfo);

	// Store the initial scale of the actor prior to shrinking
	InitialScale = ActorHit->GetActorScale3D();

	// Set the transform of the grabbed object to the scene component
	StaticMesh->SetWorldTransform(FTransform(SceneComponentToAttachTo->GetComponentRotation(), SceneComponentToAttachTo->GetComponentLocation(), ShrinkScale),false, nullptr, ETeleportType::None);

	// Check if the physics are enabled
	if (StaticMesh->IsSimulatingPhysics())
	{
		// Disable the physics
		StaticMesh->SetSimulatePhysics(false);
	}

	// Set the location of the grabbed object to the scene component's location
	SetActorLocation(SceneComponentToAttachTo->GetComponentLocation(),false, nullptr, ETeleportType::None);
}


/**
 * @brief Interface Function to release the object.
 * @param SceneComponentToDetachFrom 
 */
void AInteractableStaticObject::ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom)
{
	const FDetachmentTransformRules DetachmentTransformRules(EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld,EDetachmentRule::KeepWorld, false);

	// Detaching the grabbed object
	DetachFromActor(DetachmentTransformRules);

	// Reset the transform
	StaticMesh->SetRelativeTransform(FTransform(SceneComponentToDetachFrom->GetComponentRotation(), SceneComponentToDetachFrom->GetComponentLocation(), InitialScale),false, nullptr, ETeleportType::None);

	// Disable the physics to prevent it from falling to the ground
	StaticMesh->SetSimulatePhysics(false);
}


/**
 * @brief Interface Function to get the object's information
 * @return The information
 */
FString AInteractableStaticObject::InteractableInfo()
{
	return "Permanent block";
}

/**
 * @brief Interface Function to set time to disappear the information text
 * @return 
 */
float AInteractableStaticObject::GetTimeToDisappearInfo()
{
	return 5.f;
}

/**
 * @brief Interface function that clears the information text
 * @return 
 */
FString AInteractableStaticObject::ClearInteractableInfo()
{
	return "";
}

