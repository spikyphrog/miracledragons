// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MiracleDragons/Interfaces/Interactable.h"
#include "InteractableStaticObject.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AInteractableStaticObject : public AActor, public IInteractable
{
	GENERATED_BODY()
	// Variables
private:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh; // Static mesh for the Static Object

	UPROPERTY()
	FVector InitialScale; // Property for the initial scale of the item
	
	// Constructor
public:	
	// Sets default values for this actor's properties
	AInteractableStaticObject(); // Constructor

	// Interface Functions
	UFUNCTION()
	virtual void GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo) override;

	UFUNCTION()
	virtual void ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom) override;

	UFUNCTION()
	virtual FString InteractableInfo() override;

	UFUNCTION()
	virtual float GetTimeToDisappearInfo() override;

	UFUNCTION()
	virtual FString ClearInteractableInfo() override;
};
