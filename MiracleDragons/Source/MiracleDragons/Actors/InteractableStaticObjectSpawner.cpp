// Hristo Stoyanov 2021


#include "InteractableStaticObjectSpawner.h"

// Sets default values
AInteractableStaticObjectSpawner::AInteractableStaticObjectSpawner()
{
}

// Called when the game starts or when spawned
void AInteractableStaticObjectSpawner::BeginPlay()
{
	Super::BeginPlay();

	// Check if the spawner is allowed to spawn
	if (bAllowToSpawnItems)
	{
		// Start spawning the objects
		GetWorldTimerManager().SetTimer(SpawnObjectHandle, this, &AInteractableStaticObjectSpawner::SpawnStaticObject, SpawnDelay, true);
	}
}

/**
 * @brief Function that handles the initial spawning of the objects
 */
 void AInteractableStaticObjectSpawner::SpawnStaticObject()
{
	// Check if the currently spawned objects are less than the given cap
	if (CurrentSpawn < SpawnCap)
	{
		const FVector Location = GetActorLocation(); // Get the location of the spawner
		const FRotator Rotation = GetActorRotation(); // Get the rotation of the spawner
		
		GetWorld()->SpawnActor<AActor>(StaticObjectClass, Location, Rotation); // Spawn the objects
		
		CurrentSpawn++; // Increase the amount of currently spawned objects
	}
	// If it exceeds the cap
	else
	{
		// Clear the timer
		GetWorldTimerManager().ClearTimer(SpawnObjectHandle);
	}
}

/**
 * @brief Function that handles the respawning
 */
 void AInteractableStaticObjectSpawner::RespawnStaticObject() const
{
	const FVector Location = GetActorLocation(); // Get the location of the spawner
	const FRotator Rotation = GetActorRotation(); // Get the rotation of the spawner
	
	GetWorld()->SpawnActor<AActor>(StaticObjectClass, Location, Rotation); // Spawn the objects
}


