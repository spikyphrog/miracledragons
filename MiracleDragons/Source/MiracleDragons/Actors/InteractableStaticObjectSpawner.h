// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableStaticObject.h"

#include "InteractableStaticObjectSpawner.generated.h"


UCLASS()
class MIRACLEDRAGONS_API AInteractableStaticObjectSpawner : public AActor
{
	GENERATED_BODY()

	// Variables
public:
	UPROPERTY(EditAnywhere)
	int32 SpawnCap = 2; // Property for the maximum spawned amount objects
	
	UPROPERTY(VisibleAnywhere)
	int32 CurrentSpawn = 0; // Property for the currently spawned objects
	
	UPROPERTY(EditAnywhere)
	float SpawnDelay = .5f; // Property for the delay between the spawn of the objects
	
	UPROPERTY(EditAnywhere)
	bool bAllowToSpawnItems; //Property for allowing the spawner to spawn objects

	UPROPERTY(EditAnywhere)
	TSubclassOf<AInteractableStaticObject> StaticObjectClass; // Reference to the InteractableStaticObject class

	UPROPERTY()
	FTimerHandle SpawnObjectHandle; // Timer handle for the spawning of the objects


public:	
	// Sets default values for this actor's properties
	AInteractableStaticObjectSpawner();	// Constructor

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void SpawnStaticObject(); // Function that handles the initial spawn of the objects

	void RespawnStaticObject() const; // Function that handles the respawning of the objects
};
