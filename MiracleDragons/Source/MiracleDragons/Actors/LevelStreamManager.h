// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelStreamManager.generated.h"

UCLASS()
class MIRACLEDRAGONS_API ALevelStreamManager : public AActor
{
	GENERATED_BODY()

	// Variables
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> LevelNames; // Array for the names of the levels

public:	
	// Sets default values for this actor's properties
	ALevelStreamManager();	// Constructor
};
