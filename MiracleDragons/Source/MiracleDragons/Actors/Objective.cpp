// Hristo Stoyanov 2021


#include "Objective.h"

#include "AudioManager.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ObjectivesManager.h"
#include "MiracleDragons/Character/PlayerCharacter.h"
#include "MiracleDragons/GameModes/MainGamemode.h"

// Sets default values
AObjective::AObjective()
{
	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collider")); // Creating the box collider
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &AObjective::StartTimer); // Binding the OnComponentBeginOverlap with the StartTimer
}

// Called when the game starts or when spawned
void AObjective::BeginPlay()
{
	Super::BeginPlay();
	
	// Assigning the reference to the custom game mode
	GameMode = Cast <AMainGameMode>(GetWorld()->GetAuthGameMode());
}



void AObjective::StartTimer(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Check if the other actor is the Player and if the player has not started the timer and if the trigger is not the final objective
	if (Cast<APlayerCharacter>(OtherActor) && !GameMode->bHasStartedGameTimer && !bIsFinalObjective)
	{
		GameMode->bHasStartedGameTimer = true; // Set the started timer to true

		// Storing a reference of the Objective Manager by casting it to the only actor that implements the objective manager class
		AObjectivesManager* ObjectivesManager = Cast<AObjectivesManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ObjectivesManagerClass));

		// Check if the objective manager is valid
		if (ObjectivesManager) ObjectivesManager->bHasCompletedZone1_A = true; // 

		// Set the spawning timer
		GetWorldTimerManager().SetTimer(GameMode->StartGameTimerHandle, this, &AObjective::Timer, .001f,true);
	}

	// Check if its the final objective
	if (bIsFinalObjective)
	{
		// Clear and stop the timer
		GetWorldTimerManager().ClearTimer(GameMode->StartGameTimerHandle);

		// Set the bool to false, to display the Game over screen
		GameMode->bHasStartedGame = false;

		GameMode->AudioManager->PlaySound(GameMode->AudioManager->GetSound(2));
	}

} 

/**
 * @brief Function that handles the display of the timer
 */
void AObjective::Timer()
{
	GameMode->MilSeconds++;

	// Check if the miliseconds exceed 999
	if (GameMode->MilSeconds > 999)
	{
		GameMode->Seconds++; // Increment the seconds
		GameMode->MilSeconds = 0; // Reset the miliseconds
	}

	// Check if the seconds exceed 59
	if (GameMode->Seconds > 59)
	{
		GameMode->Minutes++; // Increment the seconds
		GameMode->Seconds = 0; // Reset the seconds
	}
}

