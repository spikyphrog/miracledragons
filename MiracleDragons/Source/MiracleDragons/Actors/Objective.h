// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Objective.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AObjective : public AActor
{
	GENERATED_BODY()
public:
	// Variables
	UPROPERTY(EditAnywhere)
	class UBoxComponent* BoxCollider; // Collider for the trigger

	UPROPERTY(EditAnywhere)
	bool bIsFinalObjective; // Property for if the trigger is the last objective
	
	UPROPERTY(VisibleAnywhere)
	class AMainGameMode* GameMode; // Reference to the custom game mode

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AObjectivesManager> ObjectivesManagerClass; // Reference to the Objectives manager class


public:	
	// Sets default values for this actor's properties
	AObjective(); // Constructor

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Function bound to the OnComponentBeginOverlap
	UFUNCTION()
	void StartTimer(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Function that handles the timer, that is displayed by the widgets
	UFUNCTION()
	void Timer();
};
