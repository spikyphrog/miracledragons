// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObjectivesManager.generated.h"

UCLASS()
class MIRACLEDRAGONS_API AObjectivesManager : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bHasCompletedZone1_A = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bHasCompletedZone1_B = false;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bHasCompletedZone2 = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bHasCompletedZone3 = false;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int32 ProgressZone1_B = 0;

	// Constructor
public:	
	// Sets default values for this actor's properties
	AObjectivesManager();

};
