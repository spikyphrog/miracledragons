// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/AudioComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

#include "MiracleDragons/GameModes/MainGameMode.h"
#include "MiracleDragons/Actors/DragonCollectable.h"
#include "MiracleDragons/Interfaces/Interactable.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
	// Setting up the spring arm
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm")); // Creating the spring arm
	SpringArm->SetupAttachment(RootComponent); // Attaching the spring arm to the root component
	SpringArm->bUsePawnControlRotation = true; // Enabling pawn control rotation

	// Setting up the camera
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;

	// Disabling some rotations to prevent weird behaviour
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 540.f, 0.f);

	ObjectPlaceholder = CreateDefaultSubobject<USceneComponent>(TEXT("Object Placeholder"));
	ObjectPlaceholder->SetupAttachment(RootComponent);
		
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent-> BindAxis(TEXT("MoveForward"), this, &APlayerCharacter::MoveForward);
	PlayerInputComponent-> BindAxis(TEXT("MoveRight"), this, &APlayerCharacter::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("TurnRate"), this, &APlayerCharacter::TurnRate);

	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this,&APlayerCharacter::LookUpRate);
	
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("Grab"), IE_Pressed, this, &APlayerCharacter::Interact);
	PlayerInputComponent->BindAction(TEXT("Release"), IE_Pressed, this, &APlayerCharacter::Release);

	PlayerInputComponent->BindAction(TEXT("Pause"), IE_Pressed, this, &APlayerCharacter::Pause);
}

/**
 * @brief Function that handles the forward movement of the character 
 * @param 
 */
 void APlayerCharacter::MoveForward(float Value)
{
	const FRotator Rotation = Controller -> GetControlRotation(); // Getting the rotation of the character
	const FRotator Yaw (0.f, Rotation.Yaw, 0.f); // Getting the forward Vector of the rotation
	const FVector Direction = FRotationMatrix(Yaw).GetUnitAxis(EAxis::X); // X axis is the forward direction in Unreal
	AddMovementInput(Direction, Value);
}

/**
 * @brief Function that handles the sideways movement of the character
 * @param Value 
 */
 void APlayerCharacter::MoveRight(float Value)
{
	const FRotator Rotation = Controller -> GetControlRotation(); // Getting the rotation of the character
	const FRotator Yaw (0.f, Rotation.Yaw, 0.f); // Getting the forward Vector of the rotation
	const FVector Direction = FRotationMatrix(Yaw).GetUnitAxis(EAxis::Y); // Y axis is for the left and right direction in Unreal
	AddMovementInput(Direction, Value);
}

/**
 * @brief Function that handles the rotation of the character
 * @param Value 
 */
 void APlayerCharacter::TurnRate(float Value)
{
	AddControllerYawInput(Value * GetWorld()->GetDeltaSeconds() * TurnRotation);
}

/**
 * @brief Function that handles the up and down rotation of the character
 * @param Value
 */
 void APlayerCharacter::LookUpRate(float Value)
{
	AddControllerPitchInput(Value * GetWorld()->GetDeltaSeconds() * LookRate);
}

/**
 * @brief Function that allows the player to interact with interactable objects
 */
 void APlayerCharacter::Interact()
{

	const FVector StartPoint = GetActorLocation(); // Get the starting point of the interactable range
	const FVector EndPoint = (GetActorForwardVector()* InteractionDistance) + StartPoint; // Calculate the end point of the interactable range

	FHitResult HitResult;
	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(this); // Add the character to the actors to ignore 

	// Sphere tracing for interactable objects
	const bool bHit = UKismetSystemLibrary::SphereTraceSingle(GetWorld(), StartPoint, EndPoint,50.f, UEngineTypes::ConvertToTraceType(ECC_Camera), false,
	ActorsToIgnore,EDrawDebugTrace::ForDuration, HitResult, true, FLinearColor::Green,FLinearColor::Red, 0.1f);

	// Check if the player hit something and if its not holding any item
	if (bHit && HeldObject == nullptr)
	{
		IInteractable* InteractableInterface = Cast<IInteractable>(HitResult.GetActor()); // Check if the hit actor implements the interface
		ADragonCollectable* IsDragon = Cast<ADragonCollectable>(HitResult.GetActor()); // Check if the hit actor is dragon collectable
		
		if (InteractableInterface)
		{
			InteractableInterface->GrabInteractable(HitResult.GetActor(), ObjectPlaceholder); // Grab the interactable object

			if (IsDragon){ return; } // if the hit actor is dragon, halt the function

			bIsHoldingObject = true; // set that the player is holding an object
			 
			HeldObject = HitResult.GetActor(); // set the held object to the interactable object

			GameMode->HeldItemInfo = InteractableInterface->InteractableInfo(); // Get the information of the interactable object

			InteractableInterface->ClearInteractableInfo(); // Call the clear interactable info to clear the message

			// Start a timer to clear the info
			GetWorldTimerManager().SetTimer(GameMode->RemoveHeldItemInfoTimerHandle, this, &APlayerCharacter::RemoveItemHeldText, InteractableInterface->GetTimeToDisappearInfo(), true);
		}
	}
}

/**
 * @brief Function that handles the releasing of an object
 */
void APlayerCharacter::Release()
{
	// Check if the player is holding an object
	if (bIsHoldingObject && HeldObject != nullptr)
	{
		// if that object is interactable, as it should be
		IInteractable* InteractableInterface = Cast<IInteractable>(HeldObject);
		
		if (InteractableInterface)
		{
			// Call the interface release interactable
			InteractableInterface->ReleaseInteractable(ObjectPlaceholder);
			HeldObject = nullptr; // Reset the held object variable
		}
	}
}

/**
 * @brief Function that handles the pausing of the game
 */
void APlayerCharacter::Pause()
{
	// Check if its not paused
	if (!GameMode->IsPaused)
	{
		UGameplayStatics::SetGamePaused(GetWorld(), true); // Pause the game
		GameMode->IsPaused = true;
		
		if (GameMode->CurrentlyPlayingSound != nullptr)
		{
			GameMode->CurrentlyPlayingSound->SetPaused(true);
		}
	}
}

/**
 * @brief Function that clears the text for the held item
 */
 void APlayerCharacter::RemoveItemHeldText()
{
	GameMode->HeldItemInfo = "";
}

