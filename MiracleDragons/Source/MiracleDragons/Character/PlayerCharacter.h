// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class MIRACLEDRAGONS_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()
	// Variables
private:
	UPROPERTY(VisibleAnywhere)
	USceneComponent* ObjectPlaceholder; // Declaring Scene component which will determine where the grabbed object will be placed
	
	UPROPERTY(VisibleAnywhere)
	class UCameraComponent* Camera; // Declaring a camera

	UPROPERTY(VisibleAnywhere)
	class USpringArmComponent* SpringArm; // Declaring a spring arm
	
	UPROPERTY(VisibleAnywhere)
	bool bIsHoldingObject = false; // Property to check if the player is holding an object
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float TurnRotation = 45.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float LookRate = 45.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float InteractionDistance = 250.f; // Declaring a distance that the player will be able to interact from

	UPROPERTY(VisibleAnywhere)
	class AMainGameMode* GameMode; // Declaring a reference to the custom game mode

	UPROPERTY()
	AActor* HeldObject = nullptr; // Declaring a reference to the object that the player will hold

	// Functions
public:
	// Sets default values for this character's properties
	APlayerCharacter(); // Constructor

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Input functions
	UFUNCTION()
	void MoveForward(float Value);
	
	UFUNCTION()
	void MoveRight(float Value);
	
	UFUNCTION()
	void TurnRate(float Value);

	UFUNCTION()
	void LookUpRate(float Value);

	UFUNCTION()
	void Interact();

	UFUNCTION()
	void Release();

	UFUNCTION()
	void Pause();
	
	// == UI ==
	void RemoveItemHeldText();
};
