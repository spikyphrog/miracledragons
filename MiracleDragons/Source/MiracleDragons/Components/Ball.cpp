// Hristo Stoyanov 2021


#include "Ball.h"

// Sets default values for this component's properties
UBall::UBall()
{
}

// Called when the game starts
void UBall::BeginPlay()
{
	Super::BeginPlay();
	
	// Assigning the reference to the custom game mode
	GameMode = Cast<AMainGameMode>(GetWorld()->GetAuthGameMode());

	// Check if the Game mode is valid
	if (GameMode)
	{
		// Assign random letter from the game mode to the ball
		DefaultLetter = GameMode->GetRandomLetter();
	}
}

