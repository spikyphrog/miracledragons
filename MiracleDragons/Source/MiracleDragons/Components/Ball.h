// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MiracleDragons/GameModes/MainGameMode.h"
#include "Ball.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MIRACLEDRAGONS_API UBall : public UActorComponent
{
	GENERATED_BODY()
	// Variables
public:
	UPROPERTY(VisibleAnywhere)
	bool IsGrabbed = false; // Property to check if the player is holding a ball
	
	UPROPERTY(EditAnywhere)
	FString DefaultLetter; // Property that would represent the letter the ball is holding

	UPROPERTY(VisibleAnywhere)
	AMainGameMode* GameMode; // Reference to the Game mode

public:	
	// Sets default values for this component's properties
	UBall(); // Constructor

	// Functions
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

};
