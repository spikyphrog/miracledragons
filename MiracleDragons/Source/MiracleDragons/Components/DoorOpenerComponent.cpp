// Hristo Stoyanov 2021

#include "DoorOpenerComponent.h"

// Sets default values for this component's properties
UDoorOpenerComponent::UDoorOpenerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}

// Called when the game starts
void UDoorOpenerComponent::BeginPlay()
{
	Super::BeginPlay();

	StartScale = GetOwner()->GetActorScale3D(); // Initialise the start scale of the object

	// Check if the door trigger array is not empty
	if (DoorTrigger.Num() != 0)
	{
		// Loop through all of the elements and set their values to the CheckIfOverlapping array
		for (int i = 0; i < DoorTrigger.Num(); i++)
		{
			CheckIfOverlapping.Add(DoorTrigger[i]->IsOverlapping());
		}
	}
}

// Called every frame
void UDoorOpenerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Set every frame the actor's scale
	FVector CurrentScale = GetOwner()->GetActorScale3D();

	// Check if the CheckIfOverlapping array is not empty
	if (CheckIfOverlapping.Num() != 0)
	{
		// Loop through all of the elements and check all the triggers if they are overlapping every frame
		for (int i = 0; i < DoorTrigger.Num(); i++)
		{
			CheckIfOverlapping[i] = DoorTrigger[i]->IsOverlapping();
		}

		// loop through all of the values in the bool array
		for (int i = 0; i < CheckIfOverlapping.Num(); i++)
		{
			// Check if it is ready to be opened
			if (CheckToOpenTheDoors(CheckIfOverlapping))
			{
				OpenDoor(CurrentScale, DeltaTime);
			}
			else
			{
				CloseDoor(CurrentScale,DeltaTime);
			}
		}
	}
	// Set the scale to the modified current scale of the actor
	GetOwner()->SetActorScale3D(CurrentScale);
}

void UDoorOpenerComponent::OpenDoor(FVector& CurrentScale, float DeltaTime)
{
	CurrentScale.X = FMath::FInterpConstantTo(CurrentScale.X, TargetScale.X, DeltaTime, Speed);
	CurrentScale.Y = FMath::FInterpConstantTo(CurrentScale.Y, TargetScale.Y, DeltaTime, Speed);
	CurrentScale.Z = FMath::FInterpConstantTo(CurrentScale.Z, TargetScale.Z, DeltaTime, Speed);
}

void UDoorOpenerComponent::CloseDoor(FVector& CurrentScale, float DeltaTime)
{
	CurrentScale.X = FMath::FInterpConstantTo(CurrentScale.X, StartScale.X, DeltaTime, Speed);
	CurrentScale.Y = FMath::FInterpConstantTo(CurrentScale.Y, StartScale.Y, DeltaTime, Speed);
	CurrentScale.Z = FMath::FInterpConstantTo(CurrentScale.Z, StartScale.Z, DeltaTime, Speed);	
}

/**
 * @brief Function that checks if all the triggers are triggered if multiple triggers is true, else if there is one trigger it would fire true which would cause to open the door
 * @param CheckToOpen 
 * @return 
 */
 bool UDoorOpenerComponent::CheckToOpenTheDoors(TArray<bool> CheckToOpen) const
{
	if (MultipleTriggers)
	{
		if (CheckToOpen[0] == true && CheckToOpen[1] == true)
		{
			return true;
		}	
	}
	else
	{
		if (CheckToOpen[0] == true)
		{
			return true;
		}
	}
	return false;
}
