// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MiracleDragons/Actors/DoorTrigger.h"
#include "DoorOpenerComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MIRACLEDRAGONS_API UDoorOpenerComponent : public UActorComponent
{
	GENERATED_BODY()

	// Variables
private:
	UPROPERTY(EditAnywhere)
	bool MultipleTriggers; // Property to allow more than one trigger to be assigned to a door opener

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	float Speed = 5; // Property to change the speed of opening of the door
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	FVector TargetScale; // Property to set to where the object should scale when open
	
	UPROPERTY()
	FVector StartScale; // Property for the initial scale of the object
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TArray<ADoorTrigger*> DoorTrigger; // Array of all the triggers that would trigger the door opener

	UPROPERTY()
	TArray<bool> CheckIfOverlapping; // Array that will store all the overlapping actors, if an actor goes off the volume they will be removed from the array
	
	//Functions
public:	
	// Sets default values for this component's properties
	UDoorOpenerComponent(); // Constructor
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	void OpenDoor(FVector& CurrentScale, float DeltaTime);
	void CloseDoor(FVector& CurrentScale, float DeltaTime);
	bool CheckToOpenTheDoors(TArray<bool> CheckToOpen) const;
};
