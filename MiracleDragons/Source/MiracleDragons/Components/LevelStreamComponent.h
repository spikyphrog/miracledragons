// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LevelStreamComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MIRACLEDRAGONS_API ULevelStreamComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName LevelToStream; // Property for the level that the component's owner should load 

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName LevelToUnload; // Property for the level that the component's owner should unload 

public:	
	// Sets default values for this component's properties
	ULevelStreamComponent();
};
