// Hristo Stoyanov 2021


#include "PlatformMover.h"

// Sets default values for this component's properties
UPlatformMover::UPlatformMover()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called every frame
void UPlatformMover::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector NewLocation = GetOwner()->GetActorLocation(); // Setting the location of the owner to their current location
	
	float DeltaX = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime)) * Amplifier; // Calculating the distance that the owner will travel
	
	NewLocation.Y += DeltaX * Speed; // Modifying the updated position
	
	RunningTime += DeltaTime; // Increase the time running
	
	GetOwner()->SetActorLocation(NewLocation); // Setting the updated location of the owner
}

