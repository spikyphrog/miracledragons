// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlatformMover.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MIRACLEDRAGONS_API UPlatformMover : public UActorComponent
{
	GENERATED_BODY()

	// Variables
	UPROPERTY()
	float RunningTime; //Counter to make the platform float

	UPROPERTY(EditAnywhere)
	float Speed = 50.f; // The speed for the movement of the platform
	
	UPROPERTY(EditAnywhere)
	float Amplifier = 5.f; // Increasing this number will increase the distance that the platform will move

public:	
	// Sets default values for this component's properties
	UPlatformMover(); // Constructor

	// Functions
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
