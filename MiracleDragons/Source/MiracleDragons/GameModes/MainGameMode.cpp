// Hristo Stoyanov 2021


#include "MainGameMode.h"

#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MiracleDragons/Actors/AudioManager.h"

void AMainGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	// Initialisation of the variables
	CollectedDragons = 0;
	CurrentLevelIndex = 0;
	HeldItemInfo = "";
	IsPaused = false;
	InitialiseMiracleWord();
	InitialiseOptionalWord();
	bHasStartedGame = true;

	
	LevelManager = Cast<ALevelStreamManager>(UGameplayStatics::GetActorOfClass(GetWorld(), LevelStreamClass)); // Getting a reference to the Level manager
	AudioManager = Cast<AAudioManager>(UGameplayStatics::GetActorOfClass(GetWorld(), AudioManagerClass)); // Getting a reference to the Audio manager
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), SubclassDragonCollectable, SpawnedCollectableDragons); // Getting all of the dragons that are spawned in the level
	MaxCollectableDragons += SpawnedCollectableDragons.Num() + 8; // Setting the variable to the length of the dragons array
	ObjectivesManager = Cast<AObjectivesManager>(UGameplayStatics::GetActorOfClass(GetWorld(), ObjectivesManagerClass));
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), TargetPointClass, AISpawnPoints);
}

/**
 * @brief Function that can be called from a blueprint as well.
 * Handling the Pausing in the game
 */
 void AMainGameMode::Resume_Implementation()
{
	UGameplayStatics::SetGamePaused(GetWorld(), false);
	IsPaused = false;
	if (CurrentlyPlayingSound != nullptr )
	{
		CurrentlyPlayingSound->SetPaused(false);
	}
}

/**
 * @brief Function that will check if the player has completed the word Miracle
 * @return 
 */
 bool AMainGameMode::CheckIfPlayerWordMatchMiracle()
{
	if (PlayerInputWord.Num() == MiracleWord.Num())
	{
		for (int i = 0; i < PlayerInputWord.Num(); i++)
		{
			if (PlayerInputWord[i] == MiracleWord[i])
			{
				bHasCompletedMiracle = true;
				return true;
			}
		}
	}
	bHasCompletedMiracle = false;
	return false;
}

/**
 * @brief Function that will check if the player has completed the word Ale
 * @return 
 */
 bool AMainGameMode::CheckIfPlayerWordMatchOptional()
{
	if (PlayerInputWord.Num() == OptionalWord.Num())
	{
		for (int i = 0; i < PlayerInputWord.Num(); i++)
		{
			if (PlayerInputWord[i] == OptionalWord[i])
			{
				bHasCompletedAle = true;
				return true;
			}
		}
	}
	bHasCompletedAle = false;
	return false;
}


/**
 * @brief Function used by the ball component to assign a random letter from the initialised words
 * @return 
 */
 FString AMainGameMode::GetRandomLetter()
{
	 return MiracleWord[FMath::RandRange(0, MiracleWord.Num() - 1)];
}

/**
 * @brief Function to check which word has the player started
 * @param Letter 
 */
 void AMainGameMode::CheckWord(FString Letter)
{
	if (Letter == "M")
	{
		bHasStartedMiracle = true;
		bHasStartedAle = false;
	}

	if(Letter == "A")
	{
		bHasStartedMiracle = false;
		bHasStartedAle = true;
	}

	if (bHasCompletedAle && Letter != "M")
	{
		bHasStartedMiracle = false;
		bHasStartedAle = true;
	}
}

/**
 * @brief Function that initialises the word Miracle
 */
 void AMainGameMode::InitialiseMiracleWord()
{
	MiracleWord.Add("M");
	MiracleWord.Add("I");
	MiracleWord.Add("R");
	MiracleWord.Add("A");
	MiracleWord.Add("C");
	MiracleWord.Add("L");
	MiracleWord.Add("E");
}

/**
 * @brief Function that initialises the word Ale
 */
void AMainGameMode::InitialiseOptionalWord()
{
	OptionalWord.Add("A");
	OptionalWord.Add("L");
	OptionalWord.Add("E");
}

/**
 * @brief Function that will be called every time a level needs to be loaded
 * @param LoadLevelName 
 */
 void AMainGameMode::LoadLevel(FName LoadLevelName)
{
	const FLatentActionInfo LatentInfo;
	UGameplayStatics::LoadStreamLevel(this, LoadLevelName, true, true, LatentInfo);
	++ CurrentLevelIndex;
}

/**
 * @brief Function that will be called whenever a level needs to be unloaded 
 * @param  
 */
 void AMainGameMode::UnloadLevel(FName UnloadLevelName) const
{
	const FLatentActionInfo LatentInfo;
	UGameplayStatics::UnloadStreamLevel(this, UnloadLevelName, LatentInfo, true);
}

/**
 * @brief Function that will start a timer, which will be done before the ball triggers the OnOverlapEnd to spawn the first Receiver before destroying the last one
 */
 void AMainGameMode::RestartBallReceivers()
{
	GetWorldTimerManager().SetTimer(HasCompletedAleHandle,this, &AMainGameMode::HasCompletedAle,.5f, false, 0.1f);
}

/**
 * @brief Get the current state of the word Miracle
 * @return Will return the letter that needs to be input
 */
FString AMainGameMode::GetWordMiracle()
{
	FString Word = "";
	for(int32 i = 0; i < MiracleWord.Num(); i++)
	{
		Word += MiracleWord[i];
	}
	return Word;
}

/**
 * @brief Get the current state of the word Ale
 * @return Will return the letter that needs to be input
 */
 FString AMainGameMode::GetWordAle()
{
	FString Word = "";
	for(int32 i = 0; i < OptionalWord.Num(); i++)
	{
		Word += OptionalWord[i];
	}
	return Word;
}

/**
 * @brief Get the current state of the current player input
 * @return
 */
FString AMainGameMode::GetWordInput()
{
	FString Word = "";
	for(int32 i = 0; i < PlayerInputWord.Num(); i++)
	{
		Word += PlayerInputWord[i];
	}
	return Word;
}

bool AMainGameMode::CheckIfCollectedAllDragons()
{
	if (CollectedDragons == MaxCollectableDragons && !bIsPlaying && CurrentlyPlayingSound == nullptr)
	{
		return true;
	}
	return false;
}

/**
 * @brief Function that will check if the player has successfully completed the word Ale
 */
 void AMainGameMode::HasCompletedAle()
{
	UnloadLevel(LevelManager->LevelNames[2]);
	CurrentLevelIndex = 0;
	GetWorldTimerManager().SetTimer(HasCompletedAle_LoadingHandle, this, &AMainGameMode::HasCompletedAle_Loading ,1.f,false, .4f);
}

/**
 * @brief A function that will handle the loading the of the first receiver
 */
 void AMainGameMode::HasCompletedAle_Loading()
{
	LoadLevel(LevelManager->LevelNames[CurrentLevelIndex]);
}
