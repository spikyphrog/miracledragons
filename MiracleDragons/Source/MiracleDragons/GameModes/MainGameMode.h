// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/GameModeBase.h"
#include "MiracleDragons/Actors/LevelStreamManager.h"
#include "MiracleDragons/Actors/ObjectivesManager.h"
#include "MainGameMode.generated.h"

class ADragonCollectable;
class AAudioManager;
UCLASS()
class MIRACLEDRAGONS_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

	// Variables
public:
	// == Collectables ==
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Collectables")
	int32 CollectedDragons; // Amount of collected dragons  (used in the UI Widget)

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category= "Collectables")
	int32 MaxCollectableDragons = 0; // Amount of spawned dragons (used in the UI Widget)
	
	UPROPERTY()
	TArray<AActor*> SpawnedCollectableDragons; // Array to store all of the spawned dragons
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> SubclassDragonCollectable; // Reference to the dragon collectable classs

	// == Pause Menu ==
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite);
	bool IsPaused; // Property for the game state.

	// == Item Info ==
	UPROPERTY()
	float TimeToItemInfoDisappear = 2.f; // Property for the delay between firing the timer
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString HeldItemInfo; // Property used to store the information of the held item by the player
	
	UPROPERTY()
	FTimerHandle RemoveHeldItemInfoTimerHandle; // TimerHandle to remove the information of the held item

	// == Level 1, Words that need to be assembled == 

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FString> MiracleWord;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FString> OptionalWord;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FString> PlayerInputWord;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHasStartedMiracle = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHasCompletedMiracle = false;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHasStartedAle = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHasCompletedAle = false;
	
	// == Bool for the level handling ==
	UPROPERTY(VisibleAnywhere)
	bool IsLevelCompleted = false;
	
	UPROPERTY(VisibleAnywhere)
	int32 CurrentLevelIndex;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ALevelStreamManager* LevelManager;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ALevelStreamManager> LevelStreamClass;

	UPROPERTY()
	FTimerHandle HasCompletedAleHandle;

	UPROPERTY()
	FTimerHandle HasCompletedAle_LoadingHandle;
	
	// == Door trigger bool ==
	UPROPERTY(VisibleAnywhere)
	bool bHasDoorTriggerAlreadyLoadedLevel = false;

	// == Game state properties ==
	UPROPERTY()
	FTimerHandle StartGameTimerHandle;

	UPROPERTY(VisibleAnywhere)
	bool bHasStartedGameTimer = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bHasStartedGame;

	// == Timer properties ==
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float MilSeconds = 0.f;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Seconds = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Minutes = 0.f;

	// == Audio Manager ==
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	AAudioManager* AudioManager; // Reference to the audio manager

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AAudioManager> AudioManagerClass; // Reference to the audio manager class
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bIsPlaying; // Property for the audio triggers to check if there is currently audio playing

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UAudioComponent* CurrentlyPlayingSound = nullptr;

	// == Objectives Manager ==
	UPROPERTY(VisibleAnywhere)
	AObjectivesManager* ObjectivesManager;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObjectivesManager> ObjectivesManagerClass;


	// == AI ==
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AActor*> AISpawnPoints;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AActor> TargetPointClass;
	// Functions
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// == Pause Menu == 
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Resume();

	// Function that checks whether the player has completed Miracle
	UFUNCTION()
	bool CheckIfPlayerWordMatchMiracle();

	// Function that checks whether the player has completed Ale
	UFUNCTION()
	bool CheckIfPlayerWordMatchOptional();

	// Function to return a random letter from the words that will be assigned in the ball component of the balls
	UFUNCTION()
	FString GetRandomLetter();

	// Function that will check which word has been started 
	UFUNCTION()
	void CheckWord(FString Letter);

	// == Level Streaming ==
	void LoadLevel(FName LoadLevelName);
	void UnloadLevel(FName UnloadLevelName) const;

	// Function that is called once the player completes Ale, in order to restart the receivers 
	UFUNCTION()
	void RestartBallReceivers();


	// Functions to get the current letter from the words
	UFUNCTION(BlueprintCallable)
	FString GetWordAle();
	
	UFUNCTION(BlueprintCallable)
	FString GetWordMiracle();

	UFUNCTION(BlueprintCallable)
	FString GetWordInput();

	UFUNCTION(BlueprintCallable)
	bool CheckIfCollectedAllDragons();
private:
	// Functions to initialise the words
	void InitialiseMiracleWord();
	void InitialiseOptionalWord();

	// Function to check if the player has completed Ale
	void HasCompletedAle();
	void HasCompletedAle_Loading();
};
