// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interactable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MIRACLEDRAGONS_API IInteractable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION()
	virtual void GrabInteractable(AActor* ActorHit, USceneComponent* SceneComponentToAttachTo) = 0;

	UFUNCTION()
	virtual void ReleaseInteractable(USceneComponent* SceneComponentToDetachFrom) = 0;

	UFUNCTION()
	virtual FString InteractableInfo() = 0;

	UFUNCTION()
	virtual float GetTimeToDisappearInfo() = 0;

	UFUNCTION()
	virtual FString ClearInteractableInfo() = 0;
};
