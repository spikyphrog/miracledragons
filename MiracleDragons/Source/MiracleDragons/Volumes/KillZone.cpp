// Hristo Stoyanov 2021


#include "KillZone.h"
#include "MiracleDragons/Actors/InteractableBallSpawner.h"
#include "MiracleDragons/Actors/InteractableCubeSpawner.h"
#include "MiracleDragons/Actors/InteractableStaticObjectSpawner.h"
#include "MiracleDragons/Actors/InteractableBall.h"
#include "MiracleDragons/Actors/InteractableObject.h"
#include "MiracleDragons/Interfaces/Interactable.h"

AKillZone::AKillZone()
{
	OnActorBeginOverlap.AddDynamic(this, &AKillZone::OnOverlapBegin); // Binding the event to the OnOverlapBegin
}

/**
 * @brief Function bound to the OnActorBeginOverlap event
 * @param OverlappedActor  
 * @param OtherActor 
 */
void AKillZone::OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor)
{
	IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
	AInteractableBall* InteractableBall = Cast<AInteractableBall>(OtherActor);
	AInteractableObject* InteractableCube = Cast<AInteractableObject>(OtherActor);
	AInteractableStaticObject* InteractableStaticObject = Cast<AInteractableStaticObject>(OtherActor);

	if (InteractableInterface)
	{
		OtherActor->Destroy();
	}

	if (InteractableStaticObject)
	{
		OtherActor->Destroy();
		if (StaticSpawner)
		{
			RespawnStaticObject();
		}
	}
	
	if (InteractableBall)
	{
		OtherActor->Destroy();
		if (BallSpawner)
		{
			RespawnBall();
		}
	}

	if (InteractableCube)
	{
		OtherActor->Destroy();
		if (CubeSpawner)
		{
		RespawnCube();
		}
	}
	
}

void AKillZone::RespawnCube() const
{
	CubeSpawner->RespawnCube();
}

void AKillZone::RespawnBall() const
{
	BallSpawner->Respawn();
}

void AKillZone::RespawnStaticObject() const
{
	StaticSpawner->RespawnStaticObject();
}

