// Hristo Stoyanov 2021

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "KillZone.generated.h"


UCLASS()
class MIRACLEDRAGONS_API AKillZone : public ATriggerBox
{
	GENERATED_BODY()

// Variables
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AInteractableCubeSpawner* CubeSpawner; // Reference to a cube spawner

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AInteractableBallSpawner* BallSpawner; // Reference to a ball spawner

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AInteractableStaticObjectSpawner* StaticSpawner; // Reference to a static object spawner
	
public:
	AKillZone(); // Constructor

private:
	// Function that is bound to the OnActorBeginOverlap
	UFUNCTION()
	void OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor);

	// Function that handles cube respawning
	UFUNCTION()
	void RespawnCube() const;

	// Function that handles ball respawning
	UFUNCTION()
	void RespawnBall() const;

	// Function that handles static object respawning
	UFUNCTION()
	void RespawnStaticObject() const;
};
