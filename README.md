This is a proof of concept for a puzzle game called Miracle Dragons,
developed for an individual project as part of my Game Design and Development BCs with Hons at the University of Bradford.

Inspired by the SCP 1762, “Where the Dragons Went”.

This project is gameplay mechanics and has no art style integrated into it.

The playable build can be found on [here](https://polygonlantern.itch.io/miracle-dragons)
with the pass "MDIndividual"

Development journey can be found on [here](https://miracledragons.wordpress.com/)

Hristo Stoyanov 2021/2022

Get in touch with me on Discord - Fblthp#9326